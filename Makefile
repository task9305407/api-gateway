run:
	go run cmd/main.go

swag:
	swag init -g api/router.go -o api/docs

proto-gen:
	./scripts/gen-proto.sh 


pull_submodule:
	git submodule update --init --recursive

update_submodule:	
	git submodule update --remote --merge
