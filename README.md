# Api Gateway

## Ishga tushurish qoidalari:
**Birinchi o'rinda api-gatewayni clone qilib olamiz quyidagi tartibda.**
```
git clone git@gitlab.com:task9305407/api-gateway.git
```

**Clone qilib olganimizdan so'ng uni ishga tushuramiz va user_serviceni ham run qilib qo'yamiz albatta.**
```
make run
```
## Dastur ishga tushganidan so'ng
**api-gateway va user service ishga tushganidan so'ng quyidagi link bilan swaggerni ochish mumkin:**
```
http://localhost:8080/v1/swagger/index.html
```




