package config

import (
	"os"

	"github.com/spf13/cast"
)

type Config struct {
	HttpPort        string
	UserServiceHost string
	UserServicePort string
	CtxTimeOut      int
	LogLevel        string
}

func Load() Config {
	c := Config{}
	c.HttpPort = cast.ToString(getOrReturnDefault("HTTP_PORT", ":8080"))
	c.UserServiceHost = cast.ToString(getOrReturnDefault("USER_SERVICE_HOST", "localhost"))
	c.UserServicePort = cast.ToString(getOrReturnDefault("USER_SERVICE_PORT", "5000"))
	c.CtxTimeOut = cast.ToInt(getOrReturnDefault("CTX_TIME_OUT", 7))
	c.LogLevel = cast.ToString(getOrReturnDefault("LOG_LEVEL", "debug"))
	return c
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}
	return defaultValue
}
