package services

import (
	"fmt"

	"gitlab.com/task/api-gateway/config"
	"gitlab.com/task/api-gateway/genproto/user"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type IServiceManager interface {
	UserService() user.UserServiceClient
}

type serviceManager struct {
	userService user.UserServiceClient
}

func (s *serviceManager) UserService() user.UserServiceClient {
	return s.userService
}

func NewServiceManager(conf *config.Config) (IServiceManager, error) {
	userServiceConn, err := grpc.Dial(
		fmt.Sprintf("%s:%s", conf.UserServiceHost, conf.UserServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		fmt.Println("error dial connected user service")
		return &serviceManager{}, err
	}

	serviceManager := &serviceManager{
		userService: user.NewUserServiceClient(userServiceConn),
	}

	return serviceManager, nil
}
