package models

type UserRegisterRequest struct {
	Name  string `json:"name"`
	Age   int64  `json:"age"`
	Phone string `json:"phone" example:"+998"`
}

type UserRegisterResponse struct {
	Id    int64  `json:"id"`
	Name  string `json:"name"`
	Age   int64  `json:"age"`
	Phone string `json:"phone"`
}

type AllUsers struct {
	Users []UserRegisterResponse 
}
