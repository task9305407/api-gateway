package v1

import (
	"context"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/task/api-gateway/api/models"
	"gitlab.com/task/api-gateway/genproto/user"
	"gitlab.com/task/api-gateway/pkg/logger"
	"gitlab.com/task/api-gateway/pkg/utils"
)

// Login user
// @Summary 		register user
// @Description 	Through this api user can register
// @Tags 			User
// @Accept 			json
// @Produce         json
// @Param        	user      body  models.UserRegisterRequest true "User"
// @Success         200					  {object} 	models.UserRegisterResponse
// @Failure         500                   {object}  models.FailureInfo
// @Failure         400                   {object}  models.FailureInfo
// @Failure         409                   {object}  models.FailureInfo
// @Router          /user/register 		  [post]
func (h *handlerV1) CreateUser(c *gin.Context) {
	var (
		body models.UserRegisterRequest
	)

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.FailureInfo{
			Code:    http.StatusBadRequest,
			Message: "Error right info",
		})
		h.log.Error("Error while binding json", logger.Error(err))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeOut))
	defer cancel()

	data, err := h.serviceManager.UserService().CreateUser(ctx, &user.UserRequest{
		Name:  body.Name,
		Age:   body.Age,
		Phone: body.Phone,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.FailureInfo{
			Code:    http.StatusInternalServerError,
			Message: "Ooops something went wrong",
		})
	}
	c.JSON(http.StatusCreated, data)
}

// Get User By Id
// @Summary 		find user by id
// @Description 	Through this api find user info by id
// @Tags 			User
// @Accept 			json
// @Produce         json
// @Param        	id                    path      int    true   "id"
// @Success         200					  {object} 	models.UserRegisterResponse
// @Failure         500                   {object}  models.FailureInfo
// @Failure         404                   {object}  models.FailureInfo
// @Failure         409                   {object}  models.FailureInfo
// @Router          /user/{id} 			  [get]
func (h *handlerV1) FindUser(c *gin.Context) {
	var (
		id = c.Param("id")
	)
	user_id, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.FailureInfo{
			Code:    http.StatusBadRequest,
			Message: "Error right info",
		})
		h.log.Error("Error while parsing id", logger.Error(err))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeOut))
	defer cancel()

	data, err := h.serviceManager.UserService().FindById(ctx, &user.UserId{
		Id: user_id,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.FailureInfo{
			Code:    http.StatusInternalServerError,
			Message: "data is not found",
			Error:   err,
		})
		h.log.Error("Error while getting user by id", logger.Error(err))
	}

	c.JSON(http.StatusOK, data)
}

// Get All Users
// @Summary 		Find All Users
// @Description 	Through this api find all user
// @Tags 			User
// @Accept 			json
// @Produce         json
// @Param           keyword        		  query string true "keyword"
// @Param        	page                  path      int    true   "page"
// @Param        	limit                 path      int    true   "limit"
// @Success         200					  {object} 	models.AllUsers
// @Failure         500                   {object}  models.FailureInfo
// @Failure         400                   {object}  models.FailureInfo
// @Failure         409                   {object}  models.FailureInfo
// @Router          /users/{keyword}/{page}/{limit}	  [get]
func (h *handlerV1) GetAllUser(c *gin.Context) {
	queryParams := c.Request.URL.Query()

	params, errString := utils.ParseQueryParams(queryParams)
	if errString != nil {
		c.JSON(http.StatusBadRequest, models.FailureInfo{
			Code:    http.StatusBadRequest,
			Message: "Enter right info",
		})
		h.log.Error("Error while gett all user")
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeOut))
	defer cancel()

	res, errs := h.serviceManager.UserService().FindAll(ctx, &user.AllUsersParamsRequest{
		Keyword: params.Search,
		Page:    params.Page,
		Limit:   params.Limit,
	})
	if errs != nil {
		c.JSON(http.StatusInternalServerError, models.FailureInfo{
			Code:    http.StatusInternalServerError,
			Message: "data is not found",
		})
		h.log.Error("error getting all user info")
		return
	}

	c.JSON(http.StatusOK, res)
}
