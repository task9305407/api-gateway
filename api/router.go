package api

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	_ "gitlab.com/task/api-gateway/api/docs"

	v1 "gitlab.com/task/api-gateway/api/v1"
	"gitlab.com/task/api-gateway/config"
	"gitlab.com/task/api-gateway/pkg/logger"
	"gitlab.com/task/api-gateway/services"
)

type Option struct {
	Conf           config.Config
	Logger         logger.Logger
	ServiceManager services.IServiceManager
}

// New ...
// @title           Task HamkorLab api
// @version         1.0

// @contact.name   Murtazoxon
// @contact.url    https://t.me/murtazoxon_gofurov
// @contact.email  gofurovmurtazoxon@gmail.com

// @host      localhost:8080
// @BasePath  /v1

// @in header
// @name Authorization
func New(option Option) *gin.Engine {
	router := gin.New()

	handlerV1 := v1.New(&v1.HandlerV1Config{
		Logger:         option.Logger,
		ServiceManager: option.ServiceManager,
		Cfg:            option.Conf,
	})
	corConfig := cors.DefaultConfig()
	corConfig.AllowAllOrigins = true
	corConfig.AllowCredentials = true
	corConfig.AllowHeaders = []string{"*"}
	corConfig.AllowBrowserExtensions = true
	corConfig.AllowMethods = []string{"*"}
	router.Use(cors.New(corConfig))

	router.MaxMultipartMemory = 8 << 20 // = 8 Mib
	api := router.Group("/v1")

	api.POST("/user/register", handlerV1.CreateUser)
	api.GET("/user/:id", handlerV1.FindUser)
	api.GET("/users/:keyword/:page/:limit", handlerV1.GetAllUser)

	url := ginSwagger.URL("swagger/doc.json")
	api.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))
	return router
}
